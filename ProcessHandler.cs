﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Monitor
{
    class ProcessHandler
    {

        public ProcessWrapper[] Processes { get; }
        public TimeSpan Timer { get; }
        public TimeSpan MaxLifetime { get; }

        public ProcessHandler(string name, uint maxLifetime, uint timer)
        {
            Processes = ProcessWrapper.FromArray(Process.GetProcessesByName(name));
            Timer = TimeSpan.FromMinutes(timer);
            MaxLifetime = TimeSpan.FromMinutes(maxLifetime);
        }

        public void Monitor()
        {
            int count = Processes.Length;
            do {
                foreach (var process in Processes)
                {
                    if (KillIfOverTime(process))
                    {
                        count--;
                        Console.WriteLine($"There is {count} process(es) left");
                    }
                }
                if (count != 0)
                {
                    Thread.Sleep(Timer);
                }
            } while (count != 0);
        }

        private bool KillIfOverTime(ProcessWrapper process)
        {
            var currentLifetime = process.GetLifetime();
            if (currentLifetime >= MaxLifetime)
            {
                process.Item.Kill();
                process.Item.WaitForExit();
                Console.WriteLine($"Process has been killed after {currentLifetime}");
                return true;
            }
            else
            {
                Console.WriteLine($"Process is currently running for {currentLifetime}");
                return false;
            }
        }
    }
}
