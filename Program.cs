﻿using System;

namespace Monitor
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var handler = CreateHandler(args);
                if (handler.Processes.Length != 0)
                {
                    Console.WriteLine($"{handler.Processes.Length} processes found");
                    handler.Monitor();
                }
                else
                {
                    Console.WriteLine("No process found with this name");
                }
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (FormatException)
            {
                Console.WriteLine("Input argumetns should have [string, uint, uint] format");
            }
            catch (OverflowException)
            {
                Console.WriteLine("Time values should be positive");
            }
        }

        static ProcessHandler CreateHandler(string[] args)
        {
            if (args.Length != 3)
            {
                throw new ArgumentException("3 argumets required");
            }
            return new ProcessHandler(args[0], uint.Parse(args[1]), uint.Parse(args[2]));
        }
    }
}
