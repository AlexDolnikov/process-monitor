﻿using System;
using System.Diagnostics;

namespace Monitor
{
    class ProcessWrapper
    {

        public Process Item { get; }

        public ProcessWrapper(Process item)
        {
            Item = item;
        }

        public static ProcessWrapper[] FromArray(Process[] processes)
        {
            var result = new ProcessWrapper[processes.Length];
            for (int i = 0; i < processes.Length; i++)
            {
                result[i] = new ProcessWrapper(processes[i]);
            }
            return result;
        }

        public TimeSpan GetLifetime()
        {
            return DateTime.Now - Item.StartTime;
        }

    }
}
